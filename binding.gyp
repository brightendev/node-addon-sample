{
   "targets": [
       {
           "target_name": "addon",
           "sources": [ "addon.cpp" ], 
           'include_dirs': [
                "C:\\opencv\\build\\include"
            ],

     
            'libraries': ['C:\\opencv\\build\\x64\\vc15\\lib\\opencv_world411']
            
       }, 
       {
           "target_name": "detect_object",
           "sources": [ "ObjectDetection.h", "ObjectDetection.cpp" ], 
           'include_dirs': [
                "C:\\opencv\\build\\include"
            ],

     
            'libraries': ['C:\\opencv\\build\\x64\\vc15\\lib\\opencv_world411']
            
       }, 
       {
           "target_name": "async_module",
           "sources": [ "AsyncSample.cpp" ], 
           'include_dirs': [
                "C:\\opencv\\build\\include"
            ],

     
            'libraries': ['C:\\opencv\\build\\x64\\vc15\\lib\\opencv_world411']
            
       }
   ]
}