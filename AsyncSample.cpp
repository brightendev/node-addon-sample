// tutorial https://nodeaddons.com/c-processing-from-node-js-part-4-asynchronous-addons/

#include <v8.h>
// #include <v8-profiler.h>
#include <node.h>
#include <uv.h>
#include <iostream>
#include <thread>
#include <chrono>
// #include <opencv2/opencv.hpp>
#include <opencv2/dnn.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/highgui.hpp>

using namespace node;
using namespace v8;
using namespace std;
using namespace cv;

struct Work
{
    uv_work_t request;
    Persistent<Function> callback;
    // Local<Uint8Array> pixels;
    unsigned char *pixels;
    unsigned int width;
    unsigned int height;

    string message;
    Mat result;
    //   std::vector<rain_result> results;
};

void WorkAsyncComplete(uv_work_t *req,int status);
void WorkAsync(uv_work_t *req);

void CalculateResultsAsync(const v8::FunctionCallbackInfo<v8::Value>&args) {
    Isolate* isolate = args.GetIsolate();
    Local<Context> context = isolate->GetCurrentContext();

    Work* work = new Work();
    work->request.data = work;

    // work->pixels = Local<Uint8Array>::Cast(args[0]);
    // work->width = Local<Uint32>::Cast(args[1]);
    // work->height = Local<Uint32>::Cast(args[2]);

    Local<Function> callback = Local<Function>::Cast(args[3]);

    Local<Uint8Array> pixels = Local<Uint8Array>::Cast(args[0]);
    void *pix = pixels->Buffer()->GetContents().Data();
    work->pixels = static_cast<unsigned char*>(pix);

    Local<Uint32> localWidth = Local<Uint32>::Cast(args[1]);
    work->width = localWidth->Uint32Value(context).FromMaybe(0);

    Local<Uint32> localHeight = Local<Uint32>::Cast(args[2]);
    work->height = localHeight->Uint32Value(context).FromMaybe(0);



    work->callback.Reset(isolate, callback);

    uv_queue_work(uv_default_loop(),&work->request, WorkAsync,WorkAsyncComplete);

    args.GetReturnValue().Set(Undefined(isolate));

}

static void WorkAsync(uv_work_t *req)
{
    Work *work = static_cast<Work *>(req->data);

    Mat mat(Size(work->width, work->height), CV_8UC4, work->pixels);
    cvtColor(mat, mat, COLOR_RGBA2BGR);
    imwrite("xxxxx.jpg", mat);

    work->result = mat;
    this_thread::sleep_for (chrono::seconds(5));
}

static void WorkAsyncComplete(uv_work_t *req,int status)
{
    Isolate * isolate = Isolate::GetCurrent();
    Work *work = static_cast<Work *>(req->data);
    
    cvtColor(work->result, work->result, COLOR_BGR2RGB);
	unsigned int length = work->result.total() * work->result.elemSize();
	Local<ArrayBuffer> res = ArrayBuffer::New(isolate, work->result.data, length);
	
    Local<Value> argv[1] = {
        res
    };
    Local<Function> func = Local<Function>::New(isolate, work->callback);
    func->Call(isolate->GetCurrentContext(), Null(isolate), 1, argv);


    work->callback.Reset();
   
    delete work;

    // cout << "finish" <<endl;
}

void Initialize(Local<Object> exports) {

    NODE_SET_METHOD(exports, "calculate_results_async", CalculateResultsAsync);
}

NODE_MODULE(addon, Initialize);