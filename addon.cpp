#include <node.h>
#include <iostream>
#include<opencv2/opencv.hpp>

using namespace v8;
using namespace cv;
using namespace std;

int x = 0;

void HelloWorld(const FunctionCallbackInfo<Value>& args) {
   cout << "Hello, world!" << endl;
}

void Sum(const FunctionCallbackInfo<Value>& args) {
    Isolate* isolate = args.GetIsolate();

    int a = 50, b = 90;
    for(int i = 0; i < 1000000; i++) {
        a += b;
    }

    auto total = Number::New(isolate, a);

    args.GetReturnValue().Set(total);
}

void iterationAdd(const FunctionCallbackInfo<Value>& args) {
    Isolate* isolate = args.GetIsolate();
    Local<Context> context = isolate->GetCurrentContext();

    int num1 = args[0]->NumberValue(context).FromMaybe(0);
    int num2 = args[1]->NumberValue(context).FromMaybe(0);
    int iteration = args[2]->NumberValue(context).FromMaybe(0);

    int result = 0;
    for(int i = 0; i < iteration; i++) {
        result += num1 + num2;
    }


    args.GetReturnValue().Set(Number::New(isolate, result));
}

void PrintName(const FunctionCallbackInfo<Value>& args) {
    v8::Isolate* isolate = args.GetIsolate();
    v8::String::Utf8Value str(isolate, args[0]);
    std::string name(*str);

    cout << "sadsad" << endl;
}

void testOpenCv(const FunctionCallbackInfo<Value>& args) {
    // Mat img = imread("D:\\Development\\workspace\\test\\test-node-cpp-addon\\untitled.png");
    // imwrite("D:\\Development\\workspace\\test\\test-node-cpp-addon\\xxxxxxxxxx.png", img);

    // cout << "Hello, worlsssssssssssd!" << endl;
    Mat mat(20, 20, CV_8UC4, Scalar(255,0,0, 255));
    // imwrite("D:\\Development\\workspace\\test\\test-node-cpp-addon\\xxxxxxxxxx.png", mat);
    int data[] = {0, 0, 255, 0, 0, 255, 255, 255, 255, 255, 255, 255};
    Mat mat2(Size(2, 2), CV_8UC3, data);
    Mat img = imread("D:\\Development\\workspace\\test\\test-node-cpp-addon\\sample.png");
    imwrite("D:\\Development\\workspace\\test\\test-node-cpp-addon\\xxxxxxxxxx.png", mat);
    cout <<  img << endl;
}

void TestUint8ClampedArray(const FunctionCallbackInfo<Value>& args) {
    Local<Uint8ClampedArray> arr = Local<Uint8ClampedArray>::Cast(args[0]);
    cout << "test uint8clampedarray" << endl;
    
    unsigned char *data = (unsigned char *) arr->Buffer()->GetContents().Data();
    const int length = arr->Length();

    cout << "length" << length << endl;
    cout << data << endl;
    cout << &data << endl;
}

void testArrayBuffer(const FunctionCallbackInfo<Value>& args) {
    Isolate* isolate = args.GetIsolate();
    Local<Uint8Array> input = Local<Uint8Array>::Cast(args[0]);

    cout << "length" << input->Length() << endl;
    // void *data = input->Buffer()->GetContents().Data();
    
    cout << "step1"  << endl;
    unsigned char *contents = (unsigned char*) input->Buffer()->GetContents().Data();

    cout << "step2"  << endl;
    // cout << contents << endl;
    // cout << "step3"  << endl;
    // cout << &contents << endl;
    // cout << "step4"  << endl;
    // cout << contents << endl;
    // cout << "step5"  << endl;
    cout << +contents[0] << endl;
    cout << "step6"  << endl;
}

void SaveImageFromPixels(const FunctionCallbackInfo<Value>& args) {
    Isolate* isolate = args.GetIsolate();
    Local<Context> context = isolate->GetCurrentContext();
    Local<Uint8Array> input = Local<Uint8Array>::Cast(args[0]);

    void *data = input->Buffer()->GetContents().Data();
    unsigned char *contents = static_cast<unsigned char*>(data);

    int width = args[1]->Uint32Value(context).FromMaybe(0);
    int height = args[2]->Uint32Value(context).FromMaybe(0);
    std::string name(*v8::String::Utf8Value(isolate, args[3]));

    Mat img(Size(width, height), CV_8UC4, contents);
    Mat output;
    cvtColor(img, output, COLOR_RGBA2BGRA);
    imwrite(name, output);

    cout << "success" << endl;
}

void testReturnArray(const FunctionCallbackInfo<Value>& args) {
    Isolate* isolate = args.GetIsolate();
    Local<Context> context = isolate->GetCurrentContext();

    Local<Uint8Array> input = Local<Uint8Array>::Cast(args[0]);
    void *data = input->Buffer()->GetContents().Data();

    Local<ArrayBuffer> buffer = ArrayBuffer::New(isolate, data, input->Length());

    args.GetReturnValue().Set(buffer);
}

void getX(const FunctionCallbackInfo<Value>& args) {
    Isolate* isolate = args.GetIsolate();
    auto val = Number::New(isolate, x);
    args.GetReturnValue().Set(val);
}

void testConvertBGR2RGBA(const FunctionCallbackInfo<Value>& args) {

    unsigned int data[] = {10, 10, 10, 20, 20, 20, 30, 30, 30, 40, 40, 40};
    Mat mat(Size(2, 2), CV_8UC3, data);
    cout <<  "original 3 ch" << endl << mat << endl;
    cvtColor(mat, mat, COLOR_BGR2RGBA);
    cout <<  "4 ch" << endl << mat << endl;
}


void Initialize(Local<Object> exports) {
    x = 99999;
    printf("initialize");
    NODE_SET_METHOD(exports, "helloWorld", HelloWorld);
    NODE_SET_METHOD(exports, "sum", Sum);
    NODE_SET_METHOD(exports, "iterationAdd", iterationAdd);
    NODE_SET_METHOD(exports, "printName", PrintName);
    NODE_SET_METHOD(exports, "testOpenCv", testOpenCv);
    NODE_SET_METHOD(exports, "saveImageFromPixels", SaveImageFromPixels);
    NODE_SET_METHOD(exports, "getX", getX);
    NODE_SET_METHOD(exports, "testUint8ClampedArray", TestUint8ClampedArray);
    NODE_SET_METHOD(exports, "testArrayBuffer", testArrayBuffer);
    NODE_SET_METHOD(exports, "testReturnArray", testReturnArray);
    NODE_SET_METHOD(exports, "testConvertBGR2RGBA", testConvertBGR2RGBA);
}

NODE_MODULE(addon, Initialize);