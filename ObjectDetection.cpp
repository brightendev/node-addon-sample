#include <node.h>

#include <fstream>
#include <sstream>
#include <iostream>

#include <opencv2/dnn.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/highgui.hpp>
#include "ObjectDetection.h"

using namespace cv;
using namespace dnn;
using namespace std;
using namespace v8;


// Initialize the parameters
float confThreshold = 0.5; // Confidence threshold
float nmsThreshold = 0.4;  // Non-maximum suppression threshold
int inpWidth = 416;  // Width of network's input image
int inpHeight = 416; // Height of network's input image
vector<string> classes;

// Remove the bounding boxes with low confidence using non-maxima suppression
void postprocess(Mat& frame, const vector<Mat>& out);

// Draw the predicted bounding box
void drawPred(int classId, float conf, int left, int top, int right, int bottom, Mat& frame);

// Get the names of the output layers
vector<cv::String> getOutputsNames(const Net& net);

void YoloObjectDetection::detect(Mat& frame)
{
	cout << "step1" << endl;
	// Load names of classes
	string classesFile = "./yolo_detection/coco.names";
	ifstream ifs(classesFile.c_str());
	string line;
	while (getline(ifs, line)) classes.push_back(line);
	cout << "step2" << endl;
	// Give the configuration and weight files for the model
	cv::String modelConfiguration = "./yolo_detection/yolov3-tiny.cfg";
	cv::String modelWeights = "./yolo_detection/yolov3-tiny.weights";
	cout << "step3" << endl;
	// Load the network
	Net net = readNetFromDarknet(modelConfiguration, modelWeights);
	net.setPreferableBackend(DNN_BACKEND_OPENCV);
	net.setPreferableTarget(DNN_TARGET_CPU);
	cout << "step4" << endl;
	string outputFile;
	Mat blob;

	outputFile = "output.jpg";
	cout << "step5" << endl;

	// frame = imread(inputFile, IMREAD_COLOR);
	imwrite("frame.jpg", frame);

	// Create a 4D blob from a frame.
	blobFromImage(frame, blob, 1 / 255.0, Size(inpWidth, inpHeight), Scalar(0, 0, 0), true, false);
	cout << "step5.1" << endl;
	// imwrite("blob.jpg", blob);
	//Sets the input to the network
	net.setInput(blob);
	cout << "step6" << endl;
	// Runs the forward pass to get output of the output layers
	vector<Mat> outs;
	cout << "step6.0" << endl;
	net.forward(outs, getOutputsNames(net));
	cout << "step7" << endl;
	// Remove the bounding boxes with low confidence
	postprocess(frame, outs);
	
	// Put efficiency information. The function getPerfProfile returns the overall time for inference(t) and the timings for each of the layers(in layersTimes)
	vector<double> layersTimes;
	double freq = getTickFrequency() / 1000;
	double t = net.getPerfProfile(layersTimes) / freq;
	string label = format("Inference time for a frame : %.2f ms", t);
	putText(frame, label, Point(0, 15), FONT_HERSHEY_SIMPLEX, 0.5, Scalar(0, 0, 255));
	cout << "step8" << endl;
	// Write the frame with the detection boxes
	Mat detectedFrame;
	frame.convertTo(detectedFrame, CV_8U);
	imwrite(outputFile, detectedFrame);
	//else video.write(detectedFrame);
}

// Remove the bounding boxes with low confidence using non-maxima suppression
void YoloObjectDetection::postprocess(Mat& frame, const vector<Mat>& outs)
{
	vector<int> classIds;
	vector<float> confidences;
	vector<Rect> boxes;

	for (size_t i = 0; i < outs.size(); ++i)
	{
		// Scan through all the bounding boxes output from the network and keep only the
		// ones with high confidence scores. Assign the box's class label as the class
		// with the highest score for the box.
		float* data = (float*)outs[i].data;
		for (int j = 0; j < outs[i].rows; ++j, data += outs[i].cols)
		{
			Mat scores = outs[i].row(j).colRange(5, outs[i].cols);
			Point classIdPoint;
			double confidence;
			// Get the value and location of the maximum score
			minMaxLoc(scores, 0, &confidence, 0, &classIdPoint);
			if (confidence > confThreshold)
			{
				int centerX = (int)(data[0] * frame.cols);
				int centerY = (int)(data[1] * frame.rows);
				int width = (int)(data[2] * frame.cols);
				int height = (int)(data[3] * frame.rows);
				int left = centerX - width / 2;
				int top = centerY - height / 2;

				classIds.push_back(classIdPoint.x);
				confidences.push_back((float)confidence);
				boxes.push_back(Rect(left, top, width, height));
			}
		}
	}

	// Perform non maximum suppression to eliminate redundant overlapping boxes with
	// lower confidences
	vector<int> indices;
	NMSBoxes(boxes, confidences, confThreshold, nmsThreshold, indices);
	for (size_t i = 0; i < indices.size(); ++i)
	{
		int idx = indices[i];
		Rect box = boxes[idx];
		drawPred(classIds[idx], confidences[idx], box.x, box.y,
			box.x + box.width, box.y + box.height, frame);
	}
}

// Draw the predicted bounding box
void YoloObjectDetection::drawPred(int classId, float conf, int left, int top, int right, int bottom, Mat& frame)
{
	//Draw a rectangle displaying the bounding box
	rectangle(frame, Point(left, top), Point(right, bottom), Scalar(255, 178, 50), 3);

	//Get the label for the class name and its confidence
	string label = format("%.2f", conf);
	if (!classes.empty())
	{
		CV_Assert(classId < (int)classes.size());
		label = classes[classId] + ":" + label;
	}

	//Display the label at the top of the bounding box
	int baseLine;
	Size labelSize = getTextSize(label, FONT_HERSHEY_SIMPLEX, 0.5, 1, &baseLine);
	top = max(top, labelSize.height);
	rectangle(frame, Point(left, top - round(1.5 * labelSize.height)), Point(left + round(1.5 * labelSize.width), top + baseLine), Scalar(255, 255, 255), FILLED);
	putText(frame, label, Point(left, top), FONT_HERSHEY_SIMPLEX, 0.75, Scalar(0, 0, 0), 1);
}

// Get the names of the output layers
vector<cv::String> YoloObjectDetection::getOutputsNames(const Net& net)
{
	cout << "step6.1" << endl;
	static vector<cv::String> names;
	cout << "step6.2" << endl;
	if (names.empty())
	{
		//Get the indices of the output layers, i.e. the layers with unconnected outputs
		vector<int> outLayers = net.getUnconnectedOutLayers();
		cout << "step6.3" << endl;
		//get the names of all the layers in the network
		vector<cv::String> layersNames = net.getLayerNames();

		// Get the names of the output layers in names
		names.resize(outLayers.size());
		for (size_t i = 0; i < outLayers.size(); ++i) {
			cout << "name=" << names[i] << endl;
			names[i] = layersNames[outLayers[i] - 1];
		}
			
	}
	cout << "step6.4" << endl;
	
	return names;
}

void detectObject(const FunctionCallbackInfo<Value>& args) {

	Isolate* isolate = args.GetIsolate();
    Local<Context> context = isolate->GetCurrentContext();
    Local<Uint8Array> input = Local<Uint8Array>::Cast(args[0]);

    void *data = input->Buffer()->GetContents().Data();
    unsigned char *contents = static_cast<unsigned char*>(data);

    int width = args[1]->Uint32Value(context).FromMaybe(0);
    int height = args[2]->Uint32Value(context).FromMaybe(0);
    std::string name(*v8::String::Utf8Value(isolate, args[3]));

    Mat mat(Size(width, height), CV_8UC4, contents);
    Mat img;
	cvtColor(mat, img, COLOR_RGBA2BGR);

	// Mat img = imread("./pedestrian03.jpg");
	imwrite("xxxxx.jpg", img);
    
	YoloObjectDetection yolo;
	yolo.detect(img);

	// cvtColor(img, img, COLOR_BGR2RGB);
	// unsigned int length = img.total() * img.elemSize();
	// Local<ArrayBuffer> bf = ArrayBuffer::New(isolate, img.data, length);
	// args.GetReturnValue().Set(bf);


}

void Initialize(Local<Object> exports) {

    printf("initialize");
    NODE_SET_METHOD(exports, "detectObject", detectObject);

    
}

NODE_MODULE(detect_object, Initialize);